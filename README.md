# MR-test-project

We want a list of students and their corresponding groups


We want it in the form of

```

## Group Xn

* John Doe
* Mary Something
* Extra Name

```

## Group A1 

* Mathias Knudsen
* Rudi Hansen
* Jakob Dahl
* Mikkel Henriksen

## Group A2 

* Nicolai Larsen
* Oscar Harttung
* Benjamin Bom Christensen

## Group A3 
* Bogdan Robert

* Andrei Romar

* Jacob Berg Koch
## Group A4

## Group B1 
* Jonas Binti Jensen

## Group B2 
* Alexander Bjørk Andersen
* Aleksis Kairiss
* Jacob Suurballe Petersen
* Dainty Lyka Bihay Olsen

## Group B3 
* Aubrey Jones
* Henrik Holm Hansen
* Aleksandra Voronina
* Camilla Errendal


## Group B4

* Ulrik Vinther-Jensen
* Mathias Andersen